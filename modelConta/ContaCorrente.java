package modelConta;


import modelCliente.PessoaFisica;
import modelCliente.PessoaJuridica;

public class ContaCorrente extends Conta {

    public ContaCorrente(PessoaFisica cliente,String nconta, String ncartao, String agencia, double limiteTransf,double saldo) {
        super(cliente,nconta, ncartao, agencia, limiteTransf,saldo);
    }
    public ContaCorrente(PessoaJuridica cliente,String nconta, String ncartao, String agencia, double limiteTransf,double saldo) {
        super(cliente,nconta, ncartao, agencia, limiteTransf,saldo);
    }
}
