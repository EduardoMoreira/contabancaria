package modelConta;

import modelCliente.Cliente;
import modelCliente.PessoaFisica;
import modelCliente.PessoaJuridica;

public class Conta {
    
    private PessoaFisica umaPessoaFisica=null;
    private PessoaJuridica umaPessoaJuridica=null;
    private Cliente cliente;
    private String nconta;
    private String ncartao;
    private String agencia;
    private double limiteTransf;
    private double saldo;
    
    public Conta(PessoaFisica umaPessoaFisica,String nconta, String ncartao, String agencia, double limiteTransf,double saldo) {
        this.saldo=saldo;
        this.nconta=nconta;
        this.ncartao = ncartao;
        this.agencia = agencia;
        this.limiteTransf = limiteTransf;
        this.umaPessoaFisica = umaPessoaFisica;
        this.cliente= umaPessoaFisica;
    }
    public Conta(PessoaJuridica umaPessoaJuridica,String nconta, String ncartao, String agencia, double limiteTransf,double saldo) {
        this.saldo=saldo;
        this.nconta=nconta;
        this.ncartao = ncartao;
        this.agencia = agencia;
        this.limiteTransf = limiteTransf;
        this.umaPessoaJuridica = umaPessoaJuridica;
        this.cliente=umaPessoaJuridica;
    }

    public String getNconta() {
        return nconta;
    }

    public void setNconta(String nconta) {
        this.nconta = nconta;
    }

    public String getNcartao() {
        return ncartao;
    }

    public void setNcartao(String ncartao) {
        this.ncartao = ncartao;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public double getLimiteTransf() {
        return limiteTransf;
    }

    public void setLimiteTransf(double limiteTransf) {
        this.limiteTransf = limiteTransf;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public PessoaFisica getUmaPessoaFisica() {
        return umaPessoaFisica;
    }

    public void setUmaPessoaFisica(PessoaFisica umaPessoaFisica) {
        this.umaPessoaFisica = umaPessoaFisica;
    }

    public PessoaJuridica getUmaPessoaJuridica() {
        return umaPessoaJuridica;
    }

    public void setUmaPessoaJuridica(PessoaJuridica umaPessoaJuridica) {
        this.umaPessoaJuridica = umaPessoaJuridica;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    
    
}
