package controller;

import java.util.ArrayList;
import modelFuncionario.Funcionario;
import modelFuncionario.Gerente;


public class ControleFuncionario {
    
    private ArrayList<Funcionario> listaFuncionario;
    private ArrayList<Gerente> listaGerentes;
    
    public ControleFuncionario(){
        listaFuncionario= new ArrayList<Funcionario>();
        listaGerentes = new ArrayList<Gerente>();
    }

    public ArrayList<Funcionario> getListaFuncionario() {
        return listaFuncionario;
    }

    public void setListaFuncionario(ArrayList<Funcionario> listaFuncionario) {
        this.listaFuncionario = listaFuncionario;
    }

    public ArrayList<Gerente> getListaGerentes() {
        return listaGerentes;
    }

    public void setListaGerentes(ArrayList<Gerente> listaGerentes) {
        this.listaGerentes = listaGerentes;
    }
    public void adicionarFuncionario(Funcionario umFuncionario){
        listaFuncionario.add(umFuncionario);
    }
    public void adicionarGerente(Gerente umGerente){
        listaGerentes.add(umGerente);
    }
    public void excluirFuncionario(Funcionario umFuncionario){
        listaFuncionario.remove(umFuncionario);
    }
    public void excluirGerente(Gerente umGerente){
        listaGerentes.remove(umGerente);
    }
    
    public Funcionario pesquisarFuncionarioNome(String umNome){
        for(Funcionario f:listaFuncionario)
            if(f.getNome().equalsIgnoreCase(umNome))
                return f;
        return null;
    }
    public Gerente pesquisarGerenteNome(String umNome){
        for(Gerente g:listaGerentes)
            if(g.getNome().equalsIgnoreCase(umNome))
                return g;
        return null;
    }
    public Funcionario pesquisarFuncionarioMatricula(String umaMatricula){
        for(Funcionario f:listaFuncionario)
            if(f.getMatricula().equalsIgnoreCase(umaMatricula))
                return f;
        return null;
    }
    public Gerente pesquisarGerenteMatricula(String umaMatricula){
        for(Gerente g:listaGerentes)
            if(g.getMatricula().equalsIgnoreCase(umaMatricula))
                return g;
        return null;
    }
    
}
