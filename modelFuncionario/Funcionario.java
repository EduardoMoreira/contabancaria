package modelFuncionario;

public class Funcionario {
    
    private String nome;
    private String telefone;
    private String matricula;
    private String senha;

    public Funcionario(String nome, String telefone, String matricula,String senha) {
        this.senha=senha;
        this.nome = nome;
        this.telefone = telefone;
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
}