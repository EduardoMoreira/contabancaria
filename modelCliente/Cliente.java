package modelCliente;

public class Cliente {
    
    private String nome;
    private String telefone1;
    private String telefone2;
    private Endereco endereco;
    
    public Cliente (String nome, String telefone1, String telefone2, Endereco endereco){
        this.nome=nome;
        this.telefone1=telefone1;
        this.telefone2=telefone2;
        this.endereco=endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone1() {
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    
}
