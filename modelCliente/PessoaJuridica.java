package modelCliente;

public class PessoaJuridica extends Cliente {
    
    private String cnpj;
    private String RazaoSocial;

    public PessoaJuridica(String nome, String telefone1, String telefone2, Endereco endereco,String cnpj, String RazaoSocial) {
        super(nome, telefone1, telefone2, endereco);
        this.cnpj = cnpj;
        this.RazaoSocial = RazaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return RazaoSocial;
    }

    public void setRazaoSocial(String RazaoSocial) {
        this.RazaoSocial = RazaoSocial;
    }
    
    
}
