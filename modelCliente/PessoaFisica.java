package modelCliente;

public class PessoaFisica extends Cliente{
    
    private String cpf;
    private String idade;

    public PessoaFisica(String cpf, String nome, String telefone1, String telefone2, Endereco endereco) {
        super(nome, telefone1, telefone2, endereco);
        this.cpf = cpf;
        this.idade = idade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
